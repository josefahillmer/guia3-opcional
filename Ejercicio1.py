# !/usr/bin/env python3
#  ! -*- coding:utf-8 -*-

# Diccionario donde se encuntran los productos de la tienda
tienda = {'Telefono' : '41',
        "Computador" : '20' ,
        "Radio" : '7',
        "Audifonos" : '19',
        "Baterias" : '24',
        "Microfono" : '13',
        "Camara" : '28',
        "Mouse" : '4' }


# Menu para ventas
def ventas():
    try:
        vent = int(input('Seleccione una opción: '))
        if vent == 1:
            buscar = input('Introdusca el nombre del producto: ')
            # Para buscar si el producto se encuntra en el diccionario
            if buscar in tienda:
                print('El producto esta disponible')
                print('Disponibilidad', tienda[buscar])
            else:
                print('No se encuntra el producto')
        elif vent == 0:
            menu()
    # Si el usuario ingresa una opcion invalida
    except ValueError:
            print("Intente nuevamente")
            ventas()



# Menu para inventario
def inventario():
    try:
        inven = int(input('Seleccione una opción: '))
        if inven == 1:
            ingresar = input('Ingrese el nuevo producto: ')
            cantidad = input('Ingrese la cantidad del producto: ')
            tienda[ingresar] = cantidad
            print(tienda)
        elif inven == 2:
            eliminar = input('Introduzca el nombre del producto que eliminara: ')
            if eliminar in tienda:
                del tienda[eliminar]
                print(tienda)
            else:
                print('No existe el producto', eliminar)
        elif inven == 3:
            modificar = input('Ingrese el producto que desea modificar: ')
            valor = input('Ingrese la nueva cantidad del producto: ')
            if modificar in tienda:
                tienda[modificar] = valor
                print(tienda)
            else:
                print('El producto no se ha encontrado')
        elif inven == 4:
            print('Listado de los productos:')
            print(tienda)
        elif inven == 0:
            menu()
    except ValueError:
        print("Intente nuevamente")
        inventario()

# Menu principal
def menu():
    try:
        print('(1) Inventario\n(2) Ventas\n(0)Salir')
        opcion = int(input("Por favor, seleccione una opción: "))
        if opcion == 1:
            print('(1) Ingresar\n(2) Eliminar\n(3) Modificar\n(4) Listado\n(0) Volver')
            inventario()
        elif opcion == 2:
            print('(1) Buscar producto\n(0) Volver')
            ventas()
        elif opcion == 3:
            print('Gracias')
    except ValueError:
        print("Intente nuevamente")
        menu()


# Funcion principal o main
if __name__ == "__main__":
    print("Bienvenida a la tienda de articulos electronicos")
    menu()
